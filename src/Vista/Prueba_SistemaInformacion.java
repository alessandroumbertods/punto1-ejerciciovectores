/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaInformacion;

/**
 *
 * @author madar
 */
public class Prueba_SistemaInformacion {
    public static void main(String[] args) {
        String url1="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/listadoProgramacionIII.csv";
        String url2="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/matematicaIII.csv";
        String url3="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/fisicaII.csv";
        
        SistemaInformacion mySistema=new SistemaInformacion(url1, url2, url3);
        System.out.println("Punto 1: " + "\n" + mySistema.toString());
        System.out.println("Punto 2: " + "\n" + "Estudiantes que tienen matriculado Programacion III, Fisica II pero no Matematicas III" + "\n"+  imprimir(mySistema.getListadoProgIII_FisicaII_NO_MatIII()));
        System.out.println("Punto 3: " + "\n" + "Estudiantes que tienen matriculado Programacion III, Matematicas pero no Fisica II" + "\n" + imprimir(mySistema.getListadoProgIII_MatIII_NO_fisII()));
        System.out.println("Punto 4: " + "\n" + "Estudiantes que tienen matriculado dos materias" + "\n" + imprimir(mySistema.getListadoVer_UNICAMENTE_dosMaterias()));
    }
    
    public static String imprimir(Estudiante [] mySistema){
        String msg = "";
        int i = 1;
        for(Estudiante x: mySistema){
            if(x != null){
                msg += i + " " + x.toString() + "\n";
                i++;
            }
        }
        return msg;
    }
}
