/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Materia;
import ufps.util.varios.ArchivoLeerURL;

/**
 * Tenemos 3 materias
 *
 * @author madarme
 */
public class SistemaInformacion {

    private Materia myMaterias[] = new Materia[3];
    private String urlProgramacionIII;
    private String urlMatematicaIII;
    private String urlFisicaII;
    private int count = 0;

    public SistemaInformacion() {
    }

    /**
     * Solución del punto 1
     *
     * @param urlProgramacionIII cadena con la url con la información de
     * estudiantes de programación III
     * @param urlMatematicaIII cadena con la url con la información de
     * estudiantes de matemáticas III
     * @param urlFisicaII cadena con la url con la información de estudiantes de
     * física II
     */
    public SistemaInformacion(String urlProgramacionIII, String urlMatematicaIII, String urlFisicaII) {
        this.urlProgramacionIII = urlProgramacionIII;
        this.urlMatematicaIII = urlMatematicaIII;
        this.urlFisicaII = urlFisicaII;

        this.crearMateria(1, "Matematicas III", this.urlMatematicaIII);
        this.crearMateria(2, "Fisica II", this.urlFisicaII);
        this.crearMateria(3, "Programacion III", this.urlProgramacionIII);
    }

    /**
     * Crea un materia
     *
     * @param codigo un entero con el código de la materia
     * @param nombreMateria una cadena que contiene el nombre de la materia
     * @param url una cadena que contiene la URL de un archivo csv con la
     * información de los estudiantes de esa materia
     */
    private void crearMateria(int codigo, String nombreMateria, String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object datos[] = archivo.leerArchivo();
        Estudiante lista[] = new Estudiante[datos.length - 1];
        for (int i = 1; i < datos.length; i++) {
            Object estudiante = datos[i];
            String result[] = estudiante.toString().split(";");
            int code = Integer.parseInt(result[0]);
            String name = result[1];
            String mail = result[2];
            Estudiante e = new Estudiante(code, name, mail);
            lista[i - 1] = e;

        }
        Materia e = new Materia(codigo, nombreMateria, lista);
        myMaterias[codigo - 1] = e;
        // :)
    }

    /**
     * Punto 2.	Crear un vector con los estudiantes que tienen matriculado
     * programación III, física II pero no Matemáticas III.
     *
     * @return un listado de estudiantes
     */
    public Estudiante[] getListadoProgIII_FisicaII_NO_MatIII() {
        Estudiante fisica[] = myMaterias[1].getMyEstudiantes();
        Estudiante programacion[] = myMaterias[2].getMyEstudiantes();
        //Estudiante lista[] = crearLista(fisica, programacion);
        return noRepetidos(crearLista(fisica, programacion));
    }

    /**
     * Punto 3.	Crear un vector con los estudiantes que tienen matriculado
     * programación III y Matemáticas III pero no física II.
     *
     * @return
     */
    public Estudiante[] getListadoProgIII_MatIII_NO_fisII() {
        Estudiante matematicas[] = myMaterias[0].getMyEstudiantes();
        Estudiante programacion[] = myMaterias[2].getMyEstudiantes();
        return noRepetidos(crearLista(matematicas, programacion));
    }

    /**
     * Punto 4.	Crear un vector con los estudiantes que están viendo dos de las
     * tres materias (Cualquiera que sea).
     *
     * @return
     */
    public Estudiante[] getListadoVer_UNICAMENTE_dosMaterias()  {
        /*
        Estudiante[] lista1 = getListadoProgIII_MatIII_NO_fisII();
        Estudiante[] lista2 = getListadoProgIII_FisicaII_NO_MatIII();
        Estudiante[] lista3 = noRepetidos(crearLista(myMaterias[1].getMyEstudiantes(), myMaterias[0].getMyEstudiantes()));
        */
        Estudiante fisica[] = myMaterias[1].getMyEstudiantes();
        Estudiante programacion[] = myMaterias[2].getMyEstudiantes();
        Estudiante matematicas[] = myMaterias[0].getMyEstudiantes();
        
        Estudiante lista1[] = crearLista(fisica, programacion);
        Estudiante lista2[] = crearLista(matematicas, programacion);
        Estudiante lista3[] = crearLista(fisica, matematicas);
        
        
        /*/
        Estudiante [] dosMaterias = new Estudiante[lista1.length+lista2.length+lista3.length];
        dosMaterias = dobleMateria(lista1, dosMaterias, count);
        dosMaterias = dobleMateria(lista2, dosMaterias, count);
        dosMaterias = dobleMateria(lista3, dosMaterias, count);
*/
        Estudiante [] dosMaterias = new Estudiante[lista1.length+lista2.length+lista3.length];
        dosMaterias = dobleMateria(lista1, dosMaterias, count);
        Estudiante list[] =dobleMateria(lista2, dosMaterias, count);
        dosMaterias = crearLista(dosMaterias, list);
        Estudiante list2[] = dobleMateria(lista3, dosMaterias, count);
        
        return noRepetidos(list2);
    }

    /**
     * Obtiene la información de todos los estudiantes por materia
     *
     * @return una cadena con los 3 listados de las materias
     */
    @Override
    public String toString() {
        String msg = "";
        for (Materia x : myMaterias) {
            msg += x.toString() + "\n";
        }
        return msg;
    }

    public Estudiante[] crearLista(Estudiante materia1[], Estudiante materia2[]) {
        Estudiante lista[] = new Estudiante[materia1.length + materia2.length];
        
        //Integer lista1[] = new Integer[9];
        //T list[] = (T[])lista1;
        int i;
        
        for (i = 0; i < materia1.length; i++) {
            lista[i] = materia1[i];
        }

        for (int j = 0; j < materia2.length; j++, i++) {
            lista[i] = materia2[j];
        }
        return lista;
    }

    public Estudiante[] noRepetidos (Estudiante[] lista) {
        for(int i=0; i < lista.length; i++){
            for (int j = i+1; j < lista.length; j++) {
                if(lista[i] != null && lista[i].equals(lista[j])){
                    lista[j] = null;
                }
                
            }
        }
        return lista;
    }
    
    public Estudiante[] dobleMateria (Estudiante[] lista, Estudiante [] dosMaterias, int count) {
        for(int i=0; i < lista.length; i++){
            for (int j = i+1; j < lista.length; j++) {
                if(lista[i] != null && lista[i].equals(lista[j])){
                    for (int k = 0; k < dosMaterias.length; k++) {
                        if(dosMaterias[k] == null){
                            dosMaterias[k] = lista[j];
                            break;
                        }   
                    }
                }
                
            }
        }
        return dosMaterias;
    }
    
}
