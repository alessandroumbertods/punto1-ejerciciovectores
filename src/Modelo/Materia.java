/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Materia {
    
    private int codMateria;
    private String nombreMateria;
    private Estudiante myEstudiantes[];

    public Materia() {
    }

    public Materia(int codMateria, String nombreMateria, Estudiante[] myEstudiantes) {
        this.codMateria = codMateria;
        this.nombreMateria = nombreMateria;
        this.myEstudiantes = myEstudiantes;
    }
    
    public int getCodMateria() {
        return codMateria;
    }

    public void setCodMateria(int codMateria) {
        this.codMateria = codMateria;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public Estudiante[] getMyEstudiantes() {
        return myEstudiantes;
    }

    public void setMyEstudiantes(Estudiante[] myEstudiantes) {
        this.myEstudiantes = myEstudiantes;
    }

    @Override
    public String toString() {
        String msg = "";
        int i = 1;
        for(Estudiante e: myEstudiantes){
            msg += i + " " + e + "\n";
            i++;
        }
        return "Materia: " + nombreMateria + " codigo de la Materia: " + codMateria + "\n" + msg;
    }
    
}
